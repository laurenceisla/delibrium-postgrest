There are two ways of running aula backend. The first one you will need Postgres database (version 11 or higher),
[PostgREST 6](https://github.com/PostgREST/postgrest/releases/tag/v6.0.2) and a webserver as Nginx or Apache.

# Running without Docker

First you will need to have an superuser on Postgres to create the users and schemas necessary for aula. You can
create them on psql with:

```
$ psql
postgres=# create user aula with superuser ;
CREATE ROLE
postgres=# alter user aula with password 'password';
ALTER ROLE
postgres=# create database aula ;
CREATE DATABASE
```

After that you can create the database running the script `create-database.sh` in this repository.
First create a configuration file as the example `delibrium.postgresql.conf.sample` and run the script with:

`$ ./create-database.sh -u aula -h localhost -p 5435 -d aula -c delibrium.postgresql.conf.sample`

Where `-u` is the user that you created in the step before (aula), `-h` is the address of the postgres server,
`-p` is the port os the postgres server, `-d` is the database name and `-c` is the configuration file.

After that edit the file `delibrium.postgrest.conf.sample' changing the parameters equal the ones creates in the
earlier steps, download the PostgREST executable and run it with:

$ postgrest delibrium.postgres.conf.sample

# Running with Docker

You can run aula backend using [Docker Compose](https://docs.docker.com/compose/) following the steps:

1. Download the latest release from Aula: https://releases.aula.de/latest.tar.bz2

2. Uncrompress the downloaded file on step 1 inside this repository folder.

> $ tar xjvf latest.tar.bz2

This will create a www/ folder.

3. Run docker-compose command inside this repository folder:

> $ docker-compose up -d

4. Access your local instance of Aula opening your browser at http://localhost:8082. You can select the school aula, username 'admin' with password 'password'.
