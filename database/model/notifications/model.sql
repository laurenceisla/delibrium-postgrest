create table if not exists aula.notifications (
    id         bigserial     primary key,
    created_at timestamptz   not null default now(),
    created_by bigint	not null default request.user_id() references aula.users (id),
    changed_at timestamptz   not null default now(),
    user_id	bigint references aula.users(id),	
    school_id   bigint references aula.school(id),
    firebase_id      text         default ''
);
