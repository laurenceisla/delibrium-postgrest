alter table aula.notifications enable row level security;

drop policy if exists notifications_select on aula.notifications;
create policy notifications_select on aula.notifications for select using (aula.is_admin(school_id) or (user_id = request.user_id() and (aula.from_school(school_id))));

drop policy if exists notifications_insert_firebase_id on aula.notifications;
create policy
  notifications_insert_firebase_id
  on aula.notifications
for insert with check (
    aula.is_admin(school_id) or 
    (aula.from_school(school_id) and
      user_id = request.user_id() and
      created_by = request.user_id())
);	

drop policy if exists notifications_update_firebase_id on aula.notifications;
create policy notifications_update_firebase_id on aula.notifications for update using (aula.is_admin(school_id) or (aula.is_owner(created_by))) with check (aula.is_admin(school_id) or (aula.is_owner(created_by)));
 
drop policy if exists notifications_delete_firebase_id on aula.notifications;
create policy notifications_delete_firebase_id on aula.notifications for delete using (aula.is_admin(school_id) or (aula.is_owner(created_by)));
