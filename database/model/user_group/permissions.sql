alter table aula.user_group enable row level security;

drop policy if exists school_admin_user_group on aula.user_group;
create policy
  school_admin_user_group
  on aula.user_group
  using
  (aula.can_change_role(school_id, group_id) or aula.from_school(school_id)) /* from_school check needed to get total_voters in quorum_info */
  with check (aula.is_admin(school_id) or aula.is_owner(user_id));
