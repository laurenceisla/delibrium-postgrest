create or replace function aula.delete_user(schoolid bigint, userid bigint)
  returns void
  language plpython3u
  set search_path = public, aula
as $$
import json

current_user_id = plpy.execute("select current_setting('request.jwt.claim.user_id', true)")[0]['current_setting']

if (str(current_user_id) == str(userid)):
  return
else:
  login_id = plpy.execute('select user_login_id from aula.users where id = {}'.format(userid))[0]['user_login_id']

  plpy.execute("update aula.users set "+
      "user_login_id = null,"+
      "first_name = 'deleted',"+
      "last_name = 'user',"+
      "username = 'deleted-' || id,"+
      "config = json_build_object('deleted', to_jsonb('t'::boolean)),"+
      "email = '',"+
      "picture = ''"+
      " where "+
      "id = {};".format(userid))
  plpy.execute('delete from aula_secure.user_login where id = {}'.format(login_id))
  plpy.execute('delete from aula.user_group where user_id = {}'.format(userid))
  return
$$;
