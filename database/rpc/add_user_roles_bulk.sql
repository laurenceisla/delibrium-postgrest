create or replace function aula.add_user_roles_bulk(schoolid bigint, userids bigint[], groupid aula.group_id, ideaspace bigint)
  returns boolean
  language plpython3u
  set search_path = public, aula
  as $$

insert_string = "INSERT INTO aula.user_group (school_id, user_id, group_id, idea_space) VALUES "
insert_string_values = []

for user_id in userids:
  string = "({},{},'{}',{})".format(schoolid, user_id, groupid, ideaspace)
  insert_string_values.append(string)

insert_string += ",".join(insert_string_values)

insert_string += "ON CONFLICT (user_id, group_id, idea_space) DO NOTHING"

plpy.execute(insert_string)

return True
$$;
