create or replace function aula.move_topic(topic_id bigint, idea_space_id bigint)
  returns void
  language plpgsql
as $$
  begin
  update aula.topic set idea_space = idea_space_id where id = topic_id;
  end
$$;

grant execute on function aula.move_topic (bigint, bigint) to aula_authenticator;
