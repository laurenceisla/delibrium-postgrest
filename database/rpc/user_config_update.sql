create or replace function aula.user_config_update(user_id bigint, key text, value text)
  returns void
  language plpgsql
as $$
  begin
  update aula.users set config = jsonb_set(config, array_append(array[]:: text[], key), cast(value as jsonb)) where id = user_id;
  end
$$;

grant execute on function aula.user_config_update(bigint, text, text) to aula_authenticator;
