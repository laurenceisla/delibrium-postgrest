\ir ./add_school.sql
\ir ./add_user.sql
\ir ./add_user_roles_bulk.sql
\ir ./change_password.sql
\ir ./change_phase.sql
\ir ./config.sql
\ir ./config_update.sql
\ir ./delete_user.sql
\ir ./delete_user_bulk.sql
\ir ./remove_user_roles_bulk.sql
\ir ./delete_idea.sql
\ir ./get_page.sql
\ir ./ideas_space_user.sql
\ir ./quorum_info.sql
\ir ./school_listing.sql
\ir ./update_page.sql
\ir ./update_user.sql
\ir ./user_listing.sql
\ir ./vote.sql
\ir ./delete_vote.sql
\ir ./user_config_update.sql
