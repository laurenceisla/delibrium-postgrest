create or replace function aula.can_change_role(school_id bigint, role aula.group_id)
  returns boolean
  language plpgsql
as $$
declare
  gid aula.group_id;
begin
  return (current_setting('request.jwt.claim.user_group', true) = 'admin') or (cast(current_setting('request.jwt.claim.school_id', true) as "numeric") = school_id and current_setting('request.jwt.claim.user_group', true) = 'school_admin' and role != 'admin');
end
$$;
